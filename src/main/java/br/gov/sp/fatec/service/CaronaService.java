package br.gov.sp.fatec.service;

import br.gov.sp.fatec.model.Carona;

import java.util.List;

public interface CaronaService {

    public Carona salvar(Carona carona);

    public void excluir(Long idCarona);

    public List<Carona> todos();

    public List<Carona> buscarPorUsuario(String nome);

    public List<Carona> buscarPorCaminho(String caminho);

    public Carona buscarPorId(Long idCarona);
}
