package br.gov.sp.fatec.service;

import br.gov.sp.fatec.model.Carona;
import br.gov.sp.fatec.model.Usuario;
import br.gov.sp.fatec.repository.CaronaRepository;
import br.gov.sp.fatec.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("caronaService")
@Transactional
public class CaronaServiceImpl implements CaronaService {


    @Autowired
    private CaronaRepository caronaRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    public void setCaronaRepository(CaronaRepository caronaRepository) {
        this.caronaRepository = caronaRepository;
    }

    public void setUsuarioRepository(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }


    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Carona salvar(Carona carona) {
        if(carona.getUsuario() != null) {
            Usuario usuario = carona.getUsuario();
            if(usuario.getId() != null) {
                usuario = usuarioRepository.findById(usuario.getId()).get();
            }
            else {
                usuario = usuarioRepository.save(usuario);
            }
        }
        return caronaRepository.save(carona);

    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void excluir(Long idCarona) {
        caronaRepository.deleteById(idCarona);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public List<Carona> todos() {
        List<Carona> carona = new ArrayList<Carona>();
        for(Carona caronaUnica: caronaRepository.findAll()) {
            carona.add(caronaUnica);
        }
        return carona;
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public List<Carona> buscarPorUsuario(String nome) {
        if(nome == null || nome.isEmpty()) {
            return todos();
        }
        return caronaRepository.findByUsuarioNome(nome);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public List<Carona> buscarPorCaminho(String caminho) {
        if(caminho == null || caminho.isEmpty()) {
            return todos();
        }
        return caronaRepository.findByCaminho(caminho);
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public Carona buscarPorId(Long idCarona) {
        Optional<Carona> carona = caronaRepository.findById(idCarona);
        if(carona.isPresent()) {
            return carona.get();
        }
        return null;
    }

}
