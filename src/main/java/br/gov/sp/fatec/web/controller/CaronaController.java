package br.gov.sp.fatec.web.controller;

import br.gov.sp.fatec.model.Carona;
import br.gov.sp.fatec.service.CaronaService;
import br.gov.sp.fatec.view.View;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collection;

@RestController
@RequestMapping(value = "/carona")
@CrossOrigin
public class CaronaController {

    @Autowired
    private CaronaService caronaService;

    public void setCaronaService(CaronaService caronaService){
        this.caronaService = caronaService;
    }

    @RequestMapping(value = "/getById/{id}", method = RequestMethod.GET)
    @JsonView(View.CaronaCompleta.class)
    public ResponseEntity<Carona> pesquisarPorId(@PathVariable("id") Long id) {
        return new ResponseEntity<Carona>(caronaService.buscarPorId(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/getAll", method = RequestMethod.GET)
    @JsonView(View.CaronaCompleta.class)
    public ResponseEntity<Collection<Carona>> getAll() {
        return new ResponseEntity<Collection<Carona>>(caronaService.todos(), HttpStatus.OK);
    }

    @RequestMapping(value = "/getCaminho/{caminho}", method = RequestMethod.GET)
    @JsonView(View.CaronaCompleta.class)
    public ResponseEntity<Collection<Carona>> getCaminho(@PathVariable("caminho") String caminho) {
        return new ResponseEntity<Collection<Carona>>(caronaService.buscarPorCaminho(caminho), HttpStatus.OK);
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @JsonView(View.CaronaCompleta.class)
    public ResponseEntity<Carona> salvar(@RequestBody Carona carona, UriComponentsBuilder uriComponentsBuilder) {
        carona = caronaService.salvar(carona);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setLocation(uriComponentsBuilder.path("/getById/" + carona.getId()).build().toUri());
        return new ResponseEntity<Carona>(carona, responseHeaders, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/excluir/{id}", method = RequestMethod.DELETE)
    @JsonView(View.CaronaCompleta.class)
    public ResponseEntity<Collection<Carona>> excluir(@PathVariable("id") Long id) {
        if (caronaService.buscarPorId(id).getId() != null){
            caronaService.excluir(id);
            return new ResponseEntity<Collection<Carona>>(caronaService.todos(), HttpStatus.OK);
        } else{
            return new ResponseEntity<Collection<Carona>>(caronaService.todos(), HttpStatus.NOT_FOUND);
        }
    }
}
