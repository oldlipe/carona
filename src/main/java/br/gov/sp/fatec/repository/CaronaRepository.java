package br.gov.sp.fatec.repository;

import br.gov.sp.fatec.model.Carona;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CaronaRepository extends CrudRepository<Carona, Long> {

    public List<Carona> findByUsuarioNome(String nome);

    public List<Carona> findByCaminho(String caminho);
}
