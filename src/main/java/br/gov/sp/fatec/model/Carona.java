package br.gov.sp.fatec.model;

import br.gov.sp.fatec.view.View;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.sql.Date;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Table(name = "CARONA")
public class Carona implements Serializable {


    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "CARONA_ID")
    @JsonView(View.CaronaCompleta.class)
    private Long id;

    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "CARONA_CAMINHO", length = 100, nullable = false)
    @JsonView(View.CaronaCompleta.class)
    private String caminho;

    @Column(name = "CARONA_DATA", nullable = false)
    @JsonView(View.CaronaCompleta.class)
    private Date mes;

    @Column(name = "CARONA_VALOR", nullable = false)
    @JsonView(View.CaronaCompleta.class)
    private float valor;

    @Column(name = "CARONA_SITUACAO", nullable = false)
    @JsonView(View.CaronaCompleta.class)
    private boolean situacao;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USR_CRIACAO_ID")
    @JsonView(View.CaronaCompleta.class)
    private Usuario usuario;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaminho() {
        return caminho;
    }

    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }

    public Date getMes() {
        return mes;
    }

    public void setMes(Date mes) {
        this.mes = mes;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public boolean isSituacao() {
        return situacao;
    }

    public void setSituacao(boolean situacao) {
        this.situacao = situacao;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}